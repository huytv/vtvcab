<?php include_once('./layouts/header.php'); ?>

<?php include_once('./layouts/page-banner.php'); ?>
<main class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php include_once('./layouts/sidebar--page-news.php'); ?>
            </div>
            <div class="col-md-8">
                <div class="article__list article__wrapper">
                    <div class="article__title">Danh sách tin tức</div>
                    <?php for ($i = 0; $i < 10; $i++) : ?>
                    <div class="articleThumb">
                        <div class="articleThumb__img">
                            <a href="./page-news--details.php"><img src="./assets/images/news-thumbnail.jpg" alt="#"></a>
                        </div>
                        <div class="articleThumb__text">
                            <h3 class="articleThumb__title"><a href="./page-news--details.php">Thông báo mời thầu gói thầu “Mua phí thường niên cho các hệ thống phần mềm”</a></h3>
                            <div class="articleThumb__excerpt">Truyền hình Cáp Việt Nam tổ chức chào hàng cạnh tranh rộng rãi trong nước gói thầu “Cung cấp phí thường niên cho các hệ thống phần mềm Oracle, Websphere, Solarwind, ServiceDesk Plus” thuộc kế hoạch “Mua phí thường niên cho các hệ thống phần mềm”.</div>
                            <a href="#" class="articleThumb__viewmore">Xem thêm</a>
                        </div>
                    </div>
                    <?php endfor; ?>
                </div>
                <?php include_once('./partials/block--breadcrumb.php'); ?>
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</main>

<?php include_once('./layouts/footer.php'); ?>
