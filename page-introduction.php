<?php include_once('./layouts/header.php'); ?>

<main class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="article__wrapper">
                    <div class="article__content">
                        <style type="text/css">
                            @import url('https://themes.googleusercontent.com/fonts/css?kit=WWfR1Kzer0wnrTJmAX08s58AUqNK1Es52VNNFzT63zk');
                            ol {
                                margin: 0;
                                padding: 0
                            }
                            
                            table td,
                            table th {
                                padding: 0
                            }
                            
                            .c1 {
                                border-right-style: solid;
                                padding: 0pt 5.8pt 0pt 5.8pt;
                                border-bottom-color: #000000;
                                border-top-width: 1pt;
                                border-right-width: 1pt;
                                border-left-color: #000000;
                                vertical-align: middle;
                                border-right-color: #000000;
                                border-left-width: 1pt;
                                border-top-style: solid;
                                background-color: #ffffff;
                                border-left-style: solid;
                                border-bottom-width: 1pt;
                                width: 86pt;
                                border-top-color: #000000;
                                border-bottom-style: solid
                            }
                            
                            .c16 {
                                border-right-style: solid;
                                padding: 0pt 5.8pt 0pt 5.8pt;
                                border-bottom-color: #000000;
                                border-top-width: 1pt;
                                border-right-width: 1pt;
                                border-left-color: #000000;
                                vertical-align: middle;
                                border-right-color: #000000;
                                border-left-width: 1pt;
                                border-top-style: solid;
                                background-color: #c0504d;
                                border-left-style: solid;
                                border-bottom-width: 1pt;
                                width: 475pt;
                                border-top-color: #000000;
                                border-bottom-style: solid
                            }
                            
                            .c18 {
                                border-right-style: solid;
                                padding: 0pt 5.8pt 0pt 5.8pt;
                                border-bottom-color: #000000;
                                border-top-width: 1pt;
                                border-right-width: 1pt;
                                border-left-color: #000000;
                                vertical-align: bottom;
                                border-right-color: #000000;
                                border-left-width: 1pt;
                                border-top-style: solid;
                                background-color: #ffffff;
                                border-left-style: solid;
                                border-bottom-width: 1pt;
                                width: 389pt;
                                border-top-color: #000000;
                                border-bottom-style: solid
                            }
                            
                            .c12 {
                                border-right-style: solid;
                                padding: 0pt 5.8pt 0pt 5.8pt;
                                border-bottom-color: #000000;
                                border-top-width: 1pt;
                                border-right-width: 1pt;
                                border-left-color: #000000;
                                vertical-align: middle;
                                border-right-color: #000000;
                                border-left-width: 1pt;
                                border-top-style: solid;
                                background-color: #ffffff;
                                border-left-style: solid;
                                border-bottom-width: 1pt;
                                width: 389pt;
                                border-top-color: #000000;
                                border-bottom-style: solid
                            }
                            
                            .c19 {
                                border-right-style: solid;
                                padding: 0pt 5.8pt 0pt 5.8pt;
                                border-bottom-color: #000000;
                                border-top-width: 1pt;
                                border-right-width: 1pt;
                                border-left-color: #000000;
                                vertical-align: middle;
                                border-right-color: #000000;
                                border-left-width: 1pt;
                                border-top-style: solid;
                                background-color: #c0504d;
                                border-left-style: solid;
                                border-bottom-width: 1pt;
                                width: 86pt;
                                border-top-color: #000000;
                                border-bottom-style: solid
                            }
                            
                            .c6 {
                                color: #000000;
                                font-weight: 400;
                                text-decoration: none;
                                vertical-align: baseline;
                                font-size: 16pt;
                                font-family: "Times New Roman";
                                font-style: normal
                            }
                            
                            .c11 {
                                color: #333333;
                                font-weight: 400;
                                text-decoration: none;
                                vertical-align: baseline;
                                font-size: 9pt;
                                font-family: "PT Sans";
                                font-style: normal
                            }
                            
                            .c3 {
                                color: #333333;
                                font-weight: 700;
                                text-decoration: none;
                                vertical-align: baseline;
                                font-size: 16.5pt;
                                font-family: "PT Sans";
                                font-style: normal
                            }
                            
                            .c0 {
                                color: #000000;
                                font-weight: 400;
                                text-decoration: none;
                                vertical-align: baseline;
                                font-size: 12pt;
                                font-family: "Times New Roman";
                                font-style: normal
                            }
                            
                            .c5 {
                                color: #000000;
                                font-weight: 400;
                                text-decoration: none;
                                vertical-align: baseline;
                                font-size: 11pt;
                                font-family: "Times New Roman";
                                font-style: normal
                            }
                            
                            .c15 {
                                color: #000000;
                                font-weight: 400;
                                text-decoration: none;
                                vertical-align: baseline;
                                font-size: 18pt;
                                font-family: "Times New Roman";
                                font-style: normal
                            }
                            
                            .c14 {
                                color: #000000;
                                font-weight: 400;
                                text-decoration: none;
                                vertical-align: baseline;
                                font-size: 14pt;
                                font-family: "Times New Roman";
                                font-style: normal
                            }
                            
                            .c2 {
                                padding-top: 0pt;
                                padding-bottom: 10pt;
                                line-height: 1.1500000000000001;
                                text-align: left
                            }
                            
                            .c20 {
                                padding-top: 0pt;
                                padding-bottom: 10pt;
                                line-height: 1.1500000000000001;
                                text-align: center
                            }
                            
                            .c21 {
                                padding-top: 0pt;
                                padding-bottom: 0pt;
                                line-height: 1.15;
                                text-align: left
                            }
                            
                            .c10 {
                                padding-top: 7.5pt;
                                padding-bottom: 7.5pt;
                                line-height: 1.1500000000000001;
                                text-align: left
                            }
                            
                            .c13 {
                                padding-top: 0pt;
                                padding-bottom: 0pt;
                                line-height: 1.0;
                                text-align: center
                            }
                            
                            .c22 {
                                padding-top: 9pt;
                                padding-bottom: 9pt;
                                line-height: 1.0;
                                text-align: left
                            }
                            
                            .c9 {
                                margin-left: -1.1pt;
                                border-spacing: 0;
                                border-collapse: collapse;
                                margin-right: auto
                            }
                            
                            .c8 {
                                padding-top: 0pt;
                                padding-bottom: 0pt;
                                line-height: 1.0;
                                text-align: left
                            }
                            
                            .c17 {
                                background-color: #ffffff;
                                max-width: 468pt;
                                padding: 27pt 72pt 72pt 72pt
                            }
                            
                            .c23 {
                                margin-left: -36pt
                            }
                            
                            .c4 {
                                height: 30pt
                            }
                            
                            .c7 {
                                height: 12pt
                            }
                            
                            .title {
                                padding-top: 24pt;
                                color: #000000;
                                font-weight: 700;
                                font-size: 36pt;
                                padding-bottom: 6pt;
                                font-family: "Times New Roman";
                                line-height: 1.1500000000000001;
                                page-break-after: avoid;
                                text-align: left
                            }
                            
                            .subtitle {
                                padding-top: 18pt;
                                color: #666666;
                                font-size: 24pt;
                                padding-bottom: 4pt;
                                font-family: "Georgia";
                                line-height: 1.1500000000000001;
                                page-break-after: avoid;
                                font-style: italic;
                                text-align: left
                            }
                            
                            li {
                                color: #000000;
                                font-size: 12pt;
                                font-family: "Times New Roman"
                            }
                            
                            p {
                                margin: 0;
                                color: #000000;
                                font-size: 12pt;
                                font-family: "Times New Roman"
                            }
                            
                            h1 {
                                padding-top: 24pt;
                                color: #000000;
                                font-weight: 700;
                                font-size: 24pt;
                                padding-bottom: 6pt;
                                font-family: "Times New Roman";
                                line-height: 1.1500000000000001;
                                page-break-after: avoid;
                                text-align: left
                            }
                            
                            h2 {
                                padding-top: 18pt;
                                color: #000000;
                                font-weight: 700;
                                font-size: 18pt;
                                padding-bottom: 4pt;
                                font-family: "Times New Roman";
                                line-height: 1.1500000000000001;
                                page-break-after: avoid;
                                text-align: left
                            }
                            
                            h3 {
                                padding-top: 14pt;
                                color: #000000;
                                font-weight: 700;
                                font-size: 14pt;
                                padding-bottom: 4pt;
                                font-family: "Times New Roman";
                                line-height: 1.1500000000000001;
                                page-break-after: avoid;
                                text-align: left
                            }
                            
                            h4 {
                                padding-top: 5pt;
                                color: #000000;
                                font-weight: 700;
                                font-size: 12pt;
                                padding-bottom: 5pt;
                                font-family: "Times New Roman";
                                line-height: 1.0;
                                text-align: left
                            }
                            
                            h5 {
                                padding-top: 11pt;
                                color: #000000;
                                font-weight: 700;
                                font-size: 11pt;
                                padding-bottom: 2pt;
                                font-family: "Times New Roman";
                                line-height: 1.1500000000000001;
                                page-break-after: avoid;
                                text-align: left
                            }
                            
                            h6 {
                                padding-top: 10pt;
                                color: #000000;
                                font-weight: 700;
                                font-size: 10pt;
                                padding-bottom: 2pt;
                                font-family: "Times New Roman";
                                line-height: 1.1500000000000001;
                                page-break-after: avoid;
                                text-align: left
                            }
                        </style>
                        <p class="c2 c7"><span class="c0"></span></p>
                        <div class="article__title ttu tac">Giới thiệu chung</div>
                        <p class="c2"><span class="c0">Truy&#7873;n h&igrave;nh c&aacute;p &#272;&#7891;ng Nai l&agrave; &#273;&#417;n v&#7883; tr&#7921;c thu&#7897;c Truy&#7873;n h&igrave;nh c&aacute;p Vi&#7879;t Nam v&#7899;i 100% v&#7889;n c&#7911;a &#272;&agrave;i Truy&#7873;n H&igrave;nh Vi&#7879;t Nam. L&agrave; &#273;&#417;n v&#7883; ti&ecirc;n phong v&agrave; l&agrave; nh&agrave; cung c&#7845;p truy&#7873;n h&igrave;nh tr&#7843; ti&#7873;n s&#7889; 1 Vi&#7879;t Nam. &#272;&#7871;n nay VTVcab &#273;&atilde; c&oacute; g&#7847;n 60 t&igrave;nh v&agrave; th&agrave;nh ph&#7889; tr&ecirc;n c&#7843; n&#432;&#7899;c, v&#7899;i &nbsp;s&#7889; thu&ecirc; bao truy&#7873;n h&igrave;nh l&#7899;n nh&#7845;t Vi&#7879;t Nam.</span></p>
                        <p
                        class="c2"><span class="c0">- Kh&ocirc;ng ng&#7915;ng m&#7903; r&#7897;ng v&ugrave;ng ph&#7911; s&oacute;ng truy&#7873;n h&igrave;nh c&aacute;p</span></p>
                        <p class="c2"><span class="c0">- Kh&ocirc;ng ng&#7915;ng gia t&#259;ng s&#7889; l&#432;&#7907;ng v&#259;n ph&ograve;ng v&agrave; chi nh&aacute;nh VTVcab</span></p>
                        <p class="c2"><span class="c0">- &#272;a d&#7841;ng nh&#7845;t Vi&#7879;t Nam v&#7873; lo&#7841;i h&igrave;nh kinh doanh d&#7883;ch v&#7909; THTT</span></p>
                        <p class="c2"><span class="c0">- Cam k&#7871;t v&#7873; b&#7843;n quy&#7873;n truy&#7873;n h&igrave;nh, s&#7843;n xu&#7845;t v&agrave; s&#7903; h&#7919;u nhi&#7873;u k&ecirc;nh THTT ch&#7845;t l&#432;&#7907;ng t&#7889;t nh&#7845;t Vi&#7879;t Nam</span></p>
                        <p
                        class="c2"><span class="c0">- T&#259;ng tr&#432;&#7903;ng v&#432;&#7907;t tr&#7897;i v&#7873; kh&aacute;ch h&agrave;ng v&agrave; kh&aacute;n gi&#7843; xem VTVcab</span></p>
                        <p class="c2"><span class="c0">- Cam k&#7871;t v&#7873; d&#7883;ch v&#7909; kh&aacute;ch h&agrave;ng, v&igrave; kh&aacute;ch h&agrave;ng VTVcab</span></p>
                        <p class="c2"><span class="c0">&Yacute; ngh&#297;a logo:</span></p>
                        <p class="c2"><span class="c0">Logo c&#7911;a truy&#7873;n h&igrave;nh c&aacute;p Vi&#7879;t Nam g&#7891;m 4 y&#7871;u t&#7889;:</span></p>
                        <p class="c2"><span class="c0">- &nbsp;VTV: Ti&#7873;n t&#7889; quan tr&#7885;ng nh&#7845;t c&#7911;a VTVcab, bi&#7875;u t&#432;&#7907;ng c&#7911;a &#272;&agrave;i truy&#7873;n h&igrave;nh Qu&#7889;c gia Vi&#7879;t Nam &ndash; &#273;&#417;n v&#7883; ch&#7911; qu&#7843;n c&#7911;a Truy&#7873;n h&igrave;nh C&aacute;p Vi&#7879;t Nam.</span></p>
                        <p
                        class="c2"><span class="c0">- &nbsp;Cab: Vi&#7871;t t&#7855;t t&#7915; Cable &ndash; h&agrave;m &yacute; t&ecirc;n th&#432;&#417;ng hi&#7879;u c&#7911;a Truy&#7873;n h&igrave;nh C&aacute;p Vi&#7879;t Nam. &#272;&acirc;y c&#361;ng l&agrave; ph&#432;&#417;ng th&#7913;c truy&#7873;n d&#7851;n c&#417; b&#7843;n nh&#7845;t c&#7911;a VTVcab.</span></p>
                        <p
                        class="c2"><span class="c0">- &nbsp;Bi&#7875;u t&#432;&#7907;ng On: L&agrave; bi&#7875;u tr&#432;ng c&#7911;a s&#7921; kh&#7903;i &#273;&#7897;ng. On multimedia &ndash; tr&ecirc;n &#273;a ph&#432;&#417;ng ti&#7879;n truy&#7873;n d&#7851;n ph&aacute;t s&oacute;ng, &#273;a d&#7883;ch v&#7909; truy&#7873;n h&igrave;nh tr&#7843; ti&#7873;n. On demand &ndash; truy&#7873;n h&igrave;nh theo y&ecirc;u c&#7847;u. On the go &ndash; m&#7885;i n&#417;i m&#7885;i l&uacute;c.</span></p>
                        <p
                        class="c2"><span class="c0">- &nbsp;Ba y&#7871;u t&#7889; tr&ecirc;n h&#7897;i t&#7909; tr&ecirc;n n&#7873;n h&igrave;nh b&igrave;nh h&agrave;nh t&#7841;o n&ecirc;n kh&#7889;i h&igrave;nh th&#7889;ng nh&#7845;t v&#7899;i &#273;&#7897; nghi&ecirc;ng theo h&#432;&#7899;ng ph&aacute;t tri&#7875;n, bi&#7875;u tr&#432;ng cho s&#7921; &#273;o&agrave;n k&#7871;t v&#432;&#417;n t&#7847;m l&#7899;n m&#7841;nh.</span></p>
                        <p
                        class="c2"><span class="c0">V&#7899;i &nbsp;th&#432;&#417;ng hi&#7879;u m&#7899;i VTVcab, Truy&#7873;n h&igrave;nh C&aacute;p Vi&#7879;t Nam &ndash; th&#432;&#417;ng hi&#7879;u th&agrave;nh vi&ecirc;n c&#7911;a VTV &ndash; &#272;&agrave;i THVN l&agrave; s&#7921; k&#7871;t tinh c&#7911;a 6 gi&aacute; tr&#7883; c&#7889;t l&otilde;i:</span></p>
                        <p
                        class="c2"><span class="c0">- H&igrave;nh &#7843;nh m&#7899;i: Th&#432;&#417;ng hi&#7879;u hi&#7879;n &#273;&#7841;i, n&#259;ng &#273;&#7897;ng v&agrave; th&acirc;n thi&#7879;n</span></p>
                        <p class="c2"><span class="c0">- Phong c&aacute;ch m&#7899;i: Chuy&ecirc;n nghi&#7879;p h&#417;n trong t&#7915;ng lo&#7841;i h&igrave;nh d&#7883;ch v&#7909;.</span></p>
                        <p class="c2"><span class="c0">- C&ocirc;ng ngh&#7879; m&#7899;i: Nghi&ecirc;n c&#7913;u, &#7913;ng d&#7909;ng c&aacute;c c&ocirc;ng ngh&#7879; hi&#7879;n &#273;&#7841;i tr&ecirc;n th&#7871; gi&#7899;i.</span></p>
                        <p class="c2"><span class="c0">- N&#7897;i dung m&#7899;i: Cung c&#7845;p c&aacute;c g&oacute;i k&ecirc;nh phong ph&uacute;, n&#7897;i dung th&#7887;a m&atilde;n nhu c&#7847;u c&#7911;a m&#7885;i &#273;&#7889;i t&#432;&#7907;ng kh&aacute;n gi&#7843;; kh&ocirc;ng ng&#7915;ng t&#259;ng k&ecirc;nh; t&#259;ng c&#432;&#7901;ng Vi&#7879;t h&oacute;a; ch&uacute; tr&#7885;ng &#273;&aacute;p &#7913;ng nhu c&#7847;u th&ocirc;ng tin, gi&#7843;i tr&iacute; c&#7911;a ng&#432;&#7901;i d&acirc;n theo &#273;&#7863;c t&iacute;nh v&ugrave;ng mi&#7873;n.</span></p>
                        <p
                        class="c2"><span class="c0">- Ch&#7845;t l&#432;&#7907;ng m&#7899;i: Kh&ocirc;ng ng&#7915;ng n&acirc;ng cao ch&#7845;t l&#432;&#7907;ng d&#7883;ch v&#7909; ng&agrave;y c&agrave;ng ho&agrave;n thi&#7879;n.</span></p>
                        <p class="c2"><span class="c0">- D&#7883;ch v&#7909; m&#7899;i: Kh&ocirc;ng ng&#7915;ng m&#7903; r&#7897;ng ph&#7841;m vi ph&#7911; s&oacute;ng truy&#7873;n h&igrave;nh c&aacute;p, &#273;a d&#7841;ng c&aacute;c lo&#7841;i h&igrave;nh d&#7883;ch v&#7909; truy&#7873;n h&igrave;nh tr&#7843; ti&#7873;n, ch&uacute; tr&#7885;ng d&#7883;ch v&#7909; sau b&aacute;n h&agrave;ng.</span></p>
                        <p
                        class="c2"><span class="c0">Th&ocirc;ng &#273;i&#7879;p:</span></p>
                        <p class="c2"><span class="c0">&quot;VTVcab - G&#7855;n k&#7871;t gia &#273;&igrave;nh&quot;</span></p>
                        <p class="c2"><span>&Yacute; ngh&#297;a c&#7911;a th&ocirc;ng &#273;i&#7879;p n&agrave;y b&#7855;t ngu&#7891;n t&#7915; s&#7921; quan tr&#7885;ng c&#7911;a gia &#273;&igrave;nh l&agrave; t&#7871; b&agrave;o c&#7911;a x&atilde; h&#7897;i, m&#7895;i th&agrave;nh vi&ecirc;n &#273;&#7873;u c&oacute; ni&#7873;m &#273;am m&ecirc; v&agrave; l&#7921;a ch&#7885;n gi&#7843;i tr&iacute; ri&ecirc;ng. VTVcab th&#7921;c hi&#7879;n s&#7913; m&#7879;nh g&#7855;n k&#7871;t m&#7885;i th&agrave;nh vi&ecirc;n trong gia &#273;&igrave;nh d&#432;&#7899;i m&#7897;t m&aacute;i nh&agrave; b&#7857;ng nh&#7919;ng gi&aacute; tr&#7883; d&#7883;ch v&#7909; &#432;u vi&#7879;t v&agrave; ti&#7879;n &iacute;ch.</span></p>
                        <p
                        class="c10"><span class="c3">VTVcab t&#7841;i &#272;&#7891;ng Nai</span></p>
                        <hr>
                        <p class="c7 c22"><span class="c11"></span></p>
                        <a id="t.54af1eb723e5ac6680a3107d042d7ed3375ca865"></a>
                        <a id="t.0"></a>
                        <table class="c9">
                        <tbody>
                        <tr class="c4">
                        <td class="c16" colspan="2" rowspan="1">
                        <p class="c13"><span class="c6">Truy&#7873;n h&igrave;nh c&aacute;p Vi&#7879;t Nam - chi nh&aacute;nh &#272;&#7891;ng Nai</span></p>
                        </td>
                        </tr>
                        <tr class="c4">
                        <td class="c12" colspan="1" rowspan="1">
                        <p class="c13"><span class="c14">&#272;&#7883;a ch&#7881;</span></p>
                        </td>
                        <td class="c1" colspan="1" rowspan="1">
                        <p class="c13"><span class="c14">&#272;i&#7879;n Tho&#7841;i</span></p>
                        </td>
                        </tr>
                        <tr class="c4">
                        <td class="c18" colspan="1" rowspan="1">
                        <p class="c8"><span class="c5">V&#259;n ph&ograve;ng TP.Bi&ecirc;n Ho&agrave; : 159 &#272;&#432;&#7901;ng &#272;&#7891;ng Kh&#7903;i - KP.6 - Ph&#432;&#7901;ng Tam Hi&#7879;p - Th&agrave;nh Ph&#7889; Bi&ecirc;n Ho&agrave; - T&#7881;nh &#272;&#7891;ng Nai</span></p>
                        </td>
                        <td class="c1" colspan="1" rowspan="5">
                        <p class="c13"><span class="c14">19001515</span></p>
                        </td>
                        </tr>
                        <tr class="c4">
                        <td class="c18" colspan="1" rowspan="1">
                        <p class="c8"><span class="c5">V&#259;n ph&ograve;ng Long Kh&aacute;nh : S&#7889; 249 H&#7891;ng Th&#7853;p T&#7921; - KP.4 - Ph&#432;&#7901;ng Xu&acirc;n Trung - Th&#7883; X&atilde; Long Kh&aacute;nh - T&#7881;nh &#272;&#7891;ng Nai</span></p>
                        </td>
                        </tr>
                        <tr class="c4">
                        <td class="c18" colspan="1" rowspan="1">
                        <p class="c8" id="h.gjdgxs"><span class="c5">V&#259;n ph&ograve;ng Tr&#7843;ng Bom : S&#7889; 135 - &#272;&#432;&#7901;ng 3/2 &ndash; KP. 5 - Th&#7883; Tr&#7845;n Tr&#7843;ng Bom - Huy&#7879;n Tr&#7843;ng Bom - T&#7881;nh &#272;&#7891;ng Nai</span></p>
                        </td>
                        </tr>
                        <tr class="c4">
                        <td class="c18" colspan="1" rowspan="1">
                        <p class="c8"><span class="c5">V&#259;n ph&ograve;ng Long Th&agrave;nh : S&#7889; 166 - &#272;&#432;&#7901;ng L&yacute; Th&aacute;i T&#7893; - &#7844;p Tr&#7847;u - X&atilde; Ph&#432;&#7899;c Thi&#7873;n - Huy&#7879;n Nh&#417;n Tr&#7841;ch - T&#7881;nh &#272;&#7891;ng Nai</span></p>
                        </td>
                        </tr>
                        <tr class="c4">
                        <td class="c18" colspan="1" rowspan="1">
                        <p class="c8"><span class="c5">V&#259;n ph&ograve;ng Xu&acirc;n L&#7897;c : 2554 &#7844;p T&acirc;n Ti&#7871;n - &nbsp;X&atilde; Xu&acirc;n Hi&#7879;p - Huy&#7879;n Xu&acirc;n L&#7897;c -T&#7881;nh &#272;&#7891;ng Nai</span></p>
                        </td>
                        </tr>
                        </tbody>
                        </table>
                        <p class="c22 c7 c23"><span class="c11"></span></p>
                        <p class="c2 c7"><span class="c14"></span></p>
                    </div>
                </div>
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</main>

<?php include_once('./layouts/footer.php'); ?>
