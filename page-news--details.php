<?php include_once('./layouts/header.php'); ?>

<?php include_once('./layouts/page-banner.php'); ?>
<main class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php include_once('./layouts/sidebar--page-services.php'); ?>
            </div>
            <div class="col-md-8">
                <div class="article__wrapper">
                    <div class="article__title">Thông báo mời thầu gói thầu “Mua phí thường niên cho các hệ thống phần mềm”</div>  
                    <div class="article__content">
                        <p>Truyền hình Cáp Việt Nam tổ chức chào hàng cạnh tranh rộng rãi trong nước gói thầu “Cung cấp phí thường niên cho các hệ thống phần mềm Oracle, Websphere, Solarwind, ServiceDesk Plus” thuộc kế hoạch “Mua phí thường niên cho các hệ thống phần mềm”.
                        Xin mời các nhà thầu có đủ điều kiện và năng lực tới tham dự chào hàng cho gói thầu trên. Nhà thầu có thể tìm hiểu thêm thông tin và mua hồ sơ yêu cầu tại:
                        TỔNG CÔNG TY TRUYỀN HÌNH CÁP VIỆT NAM
                        Địa chỉ: Số 844 Đường La Thành - Ba Đình - Hà Nội- Việt Nam.
                        VPGD: Tầng 8 toà nhà VIT Tower số 519 Kim Mã - Ba Đình
                        - Hà Nội - Việt Nam.
                        Điện thoại: 04 37717675; Fax : 04 38317364
                        Nhà thầu sẽ được mua một bộ hồ sơ yêu cầu với một khoản lệ phí (không hoàn lại) là 500.000 đồng (Năm trăm nghìn đồng) tại địa chỉ trên.
                        Trước khi nộp HSĐX nhà thầu phải đóng đảm bảo dự thầu với số tiền: 22.000.000 đồng (Hai mươi hai triệu đồng).
                        - Thời gian bán hồ sơ yêu cầu: từ  8 giờ, ngày 14 tháng 6 năm 2017 đến trước 09 giờ 00 phút, ngày 22 tháng 6 năm 2017 (trong giờ hành chính).
                        - Thời gian đóng thầu: 09h00 ngày 22 tháng 6 năm 2017.
                        - Thời gian mở thầu: 09h30 ngày 22 tháng 6 năm 2017.</p>
                        <br>
                        <p><img src="./assets/images/home/banner-demo1.png" alt=""></p>
                        <br>
                        <p>Truyền hình Cáp Việt Nam tổ chức chào hàng cạnh tranh rộng rãi trong nước gói thầu “Cung cấp phí thường niên cho các hệ thống phần mềm Oracle, Websphere, Solarwind, ServiceDesk Plus” thuộc kế hoạch “Mua phí thường niên cho các hệ thống phần mềm”.
                        Xin mời các nhà thầu có đủ điều kiện và năng lực tới tham dự chào hàng cho gói thầu trên. Nhà thầu có thể tìm hiểu thêm thông tin và mua hồ sơ yêu cầu tại:
                        TỔNG CÔNG TY TRUYỀN HÌNH CÁP VIỆT NAM
                        Địa chỉ: Số 844 Đường La Thành - Ba Đình - Hà Nội- Việt Nam.
                        VPGD: Tầng 8 toà nhà VIT Tower số 519 Kim Mã - Ba Đình
                        - Hà Nội - Việt Nam.
                        Điện thoại: 04 37717675; Fax : 04 38317364
                        Nhà thầu sẽ được mua một bộ hồ sơ yêu cầu với một khoản lệ phí (không hoàn lại) là 500.000 đồng (Năm trăm nghìn đồng) tại địa chỉ trên.
                        Trước khi nộp HSĐX nhà thầu phải đóng đảm bảo dự thầu với số tiền: 22.000.000 đồng (Hai mươi hai triệu đồng).
                        - Thời gian bán hồ sơ yêu cầu: từ  8 giờ, ngày 14 tháng 6 năm 2017 đến trước 09 giờ 00 phút, ngày 22 tháng 6 năm 2017 (trong giờ hành chính).
                        - Thời gian đóng thầu: 09h00 ngày 22 tháng 6 năm 2017.
                        - Thời gian mở thầu: 09h30 ngày 22 tháng 6 năm 2017.</p>
                    </div>
                </div>
                <div class="article__list article__wrapper">
                    <div class="article__title">Tin liên quan</div>
                    <?php for ($i = 0; $i < 3; $i++) : ?>
                    <div class="articleThumb">
                        <div class="articleThumb__img">
                            <a href="./page-news--details.php"><img src="./assets/images/news-thumbnail.jpg" alt="#"></a>
                        </div>
                        <div class="articleThumb__text">
                            <h3 class="articleThumb__title"><a href="./page-news--details.php">Thông báo mời thầu gói thầu “Mua phí thường niên cho các hệ thống phần mềm”</a></h3>
                            <div class="articleThumb__excerpt">Truyền hình Cáp Việt Nam tổ chức chào hàng cạnh tranh rộng rãi trong nước gói thầu “Cung cấp phí thường niên cho các hệ thống phần mềm Oracle, Websphere, Solarwind, ServiceDesk Plus” thuộc kế hoạch “Mua phí thường niên cho các hệ thống phần mềm”.</div>
                            <a href="#" class="articleThumb__viewmore">Xem thêm</a>
                        </div>
                    </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</main>

<?php include_once('./layouts/footer.php'); ?>
