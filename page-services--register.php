<?php include_once('./layouts/header.php'); ?>

<main class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php include_once('./layouts/sidebar--page-services.php'); ?>
            </div>
            <div class="col-md-8 col-md-offset-1">
                <?php include_once('./partials/block--form-register.php'); ?>
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</main>

<?php include_once('./layouts/footer.php'); ?>
