<?php include_once('./layouts/header.php'); ?>

<main class="main">
    <?php include_once('./layouts/banner.php'); ?>
    <?php include_once('./partials/home/services.php'); ?>
    <?php include_once('./partials/home/features.php'); ?>
    <?php include_once('./partials/home/news.php'); ?>
    <?php include_once('./partials/home/map.php'); ?>
    <?php //include_once('./partials/home/partner.php'); ?>
</main>

<?php include_once('./layouts/footer.php'); ?>