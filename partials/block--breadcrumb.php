<div class="c-pager">
    <ul class="page-numbers">
        <li><a class="prev page-numbers" href="#"><i class="fa fa-angle-left"></i></a></li>
        <li><a class="page-numbers" href="#">1</a></li>
        <li><span class="page-numbers current">2</span></li>
        <li><a class="page-numbers" href="#">...</a></li>
        <li><a class="page-numbers" href="#">10</a></li>
        <li><a class="next page-numbers" href="#"><i class="fa fa-angle-right"></i></a></li>
    </ul>
</div>
