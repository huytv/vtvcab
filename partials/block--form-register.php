<form class="serviceRegister" action="#">
    <div class="serviceRegister__label">
        <span>Đăng ký dịch vụ</span>
        <div class="form-group tac">
            <p>Chúng tôi sẽ liên hệ với bạn khi nhận được thông tin.</p>
        </div>
    </div>
    <div class="input-group">
        <lable class="input-label">Tên người đăng ký (<span class="red">*</span>)</lable>
        <input type="text" class="form-control" placeholder="Nhập tên">
    </div>
    <div class="input-group">
        <lable class="input-label">Địa chỉ</lable> (<span class="red">*</span>)</lable>
        <input type="text" class="form-control" placeholder="Nhập địa chỉ">
    </div>
    <div class="input-group">
        <lable class="input-label">Số điện thoại (<span class="red">*</span>)</lable>
        <input type="text" class="form-control" placeholder="Nhập số điện thoại">
    </div>
    <div>Chọn Loại dịch vụ: (<span class="red">*</span>)</div>
    <div class="row">
        <div class="col-md-6">
            <label class="checkbox-inline">
                <input type="checkbox" value="">Truyền hình cáp
            </label>
        </div>
        <div class="col-md-6">
            <label class="checkbox-inline">
                <input type="checkbox" value="">Internet
            </label>
        </div>
        <div class="col-md-6">
            <label class="checkbox-inline">
                <input type="checkbox" value="">Truyền hình HD
            </label>
        </div>
        <div class="col-md-6">
            <label class="checkbox-inline">
                <input type="checkbox" value="">VTVcab ON
            </label>
        </div>
    </div>
    <div class="form-group">
        <label for="exampleTextarea">Nội dung tin nhắn</label>
        <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
    </div>
    <button type="submit" class="btn btn-default">Gửi Yêu Cầu</button>
    <div class="form-group">
        <p>Ghi chú: Phần (<span class="red">*</span>) là phần bắt buộc</p>
    </div>
</form>
