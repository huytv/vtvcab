<section class="homeNews">
    <div class="container">
        <div class="homeNews__label">Tin tức và sự kiện</div>
        <div class="homeNews__list">

            <a href="#" class="newsThumbnails">
                <div class="newsThumbnails__img">
                    <img src="./assets/images/home/tinh-nang/demo3.jpg" alt="TRUYỀN HÌNH CÁP HD">
                    <span class="newsThumbnails__date">11/7</span>
                </div>
                <div class="newsThumbnails__text">
                    <h3 class="newsThumbnails__title">Đài vtc chính thức phủ sóng truyền hình số DVG-T2 ở Nghệ An và Hà Tĩnh</h3>
                    <div class="newsThumbnails__excerpt">Vào ngày 11/6/2017, Đài Truyền hình Kỹ thuật số VTC sẽ khai trương trạm phát sóng truyền hình...</div>
                </div>
            </a>

            <a href="#" class="newsThumbnails">
                <div class="newsThumbnails__img">
                    <img src="./assets/images/home/tinh-nang/demo1.jpg" alt="TRUYỀN HÌNH CÁP HD">
                    <span class="newsThumbnails__date">10/7</span>
                </div>
                <div class="newsThumbnails__text">
                    <h3 class="newsThumbnails__title">Đài vtc chính thức phủ sóng truyền hình số DVG-T2 ở Nghệ An và Hà Tĩnh</h3>
                    <div class="newsThumbnails__excerpt">Vào ngày 11/6/2017, Đài Truyền hình Kỹ thuật số VTC sẽ khai trương trạm phát sóng truyền hình...</div>
                </div>
            </a>

            <a href="#" class="newsThumbnails">
                <div class="newsThumbnails__img">
                    <img src="./assets/images/home/tinh-nang/demo2.jpg" alt="TRUYỀN HÌNH CÁP HD">
                    <span class="newsThumbnails__date">9/7</span>
                </div>
                <div class="newsThumbnails__text">
                    <h3 class="newsThumbnails__title">Đài vtc chính thức phủ sóng truyền hình số DVG-T2 ở Nghệ An và Hà Tĩnh</h3>
                    <div class="newsThumbnails__excerpt">Vào ngày 11/6/2017, Đài Truyền hình Kỹ thuật số VTC sẽ khai trương trạm phát sóng truyền hình...</div>
                </div>
            </a>

            <a href="#" class="newsThumbnails">
                <div class="newsThumbnails__img">
                    <img src="./assets/images/home/tinh-nang/demo4.jpg" alt="TRUYỀN HÌNH CÁP HD">
                    <span class="newsThumbnails__date">8/7</span>
                </div>
                <div class="newsThumbnails__text">
                    <h3 class="newsThumbnails__title">Đài vtc chính thức phủ sóng truyền hình số DVG-T2 ở Nghệ An và Hà Tĩnh</h3>
                    <div class="newsThumbnails__excerpt">Vào ngày 11/6/2017, Đài Truyền hình Kỹ thuật số VTC sẽ khai trương trạm phát sóng truyền hình...</div>
                </div>
            </a>

        </div>
        <div>
            <a href="#" class="homeNews__btn">Xem thêm</a>
        </div>
    </div>
</section>