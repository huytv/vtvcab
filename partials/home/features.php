<section class="homeFeature">
    <div class="homeFeature__label">Tính năng</div>

    <div class="homeFeature__wrapper clearfix">
        <div class="homeFeature__item">
            <img src="./assets/images/home/tinh-nang/demo1.jpg" alt="Internet cá nhân giá rẻ"/>
            <div class="homeFeature__title">Internet cá nhân giá rẻ</div>
        </div>
        <div class="homeFeature__item">
            <img src="./assets/images/home/tinh-nang/demo2.jpg" alt="210 kênh với 70 kênh HD"/>
            <div class="homeFeature__title">210 kênh với 70 kênh HD</div>
        </div>
        <div class="homeFeature__item">
            <img src="./assets/images/home/tinh-nang/demo3.jpg" alt="Hỗ trợ âm thanh 5.1"/>
            <div class="homeFeature__title">Hỗ trợ âm thanh 5.1</div>
        </div>
        <div class="homeFeature__item">
            <img src="./assets/images/home/tinh-nang/demo4.jpg" alt="Hỗ trợ K+"/>
            <div class="homeFeature__title">Hỗ trợ K+</div>
        </div>
        <div class="homeFeature__item">
            <img src="./assets/images/home/tinh-nang/demo5.jpg" alt="Ổn định với mọi thời tiết"/>
            <div class="homeFeature__title">Ổn định với mọi thời tiết</div>
        </div>
        <div class="homeFeature__item">
            <img src="./assets/images/home/tinh-nang/demo1.jpg" alt="Lắp nhanh trong 24 giờ"/>
            <div class="homeFeature__title">Lắp nhanh trong 24 giờ</div>
        </div>
        <div class="homeFeature__item">
            <img src="./assets/images/home/tinh-nang/demo2.jpg" alt="Đầu thu HD thế hệ mới"/>
            <div class="homeFeature__title">Đầu thu HD thế hệ mới</div>
        </div>
        <div class="homeFeature__item">
            <img src="./assets/images/home/tinh-nang/demo3.jpg" alt="Tiết kiệm tối đa chi phí"/>
            <div class="homeFeature__title">Tiết kiệm tối đa chi phí</div>
        </div>
        <div class="homeFeature__item">
            <img src="./assets/images/home/tinh-nang/demo4.jpg" alt="Đa dạng nhóm kênh"/>
            <div class="homeFeature__title">Đa dạng nhóm kênh</div>
        </div>
        <div class="homeFeature__item">
            <img src="./assets/images/home/tinh-nang/demo5.jpg" alt="Truyền hình cáp chuẩn HD"/>
            <div class="homeFeature__title">Truyền hình cáp chuẩn HD</div>
        </div>
        <div class="homeFeature__item">
            <img src="./assets/images/home/tinh-nang/demo1.jpg" alt="Truyền hình xem lại"/>
            <div class="homeFeature__title">Truyền hình xem lại</div>
        </div>
        <div class="homeFeature__item">
            <img src="./assets/images/home/tinh-nang/demo3.jpg" alt="Truyền hình xem lại"/>
            <div class="homeFeature__title">Truyền hình xem lại</div>
        </div>
    </div>
</section>