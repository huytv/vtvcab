<section class="homeMap">
    <img class="homeMap__img" src="./assets/images/home/map.jpg" alt="#" />
    <a class="homeMap__marker" href="https://goo.gl/maps/24h7zDeeRnR2" target="_blank">
        <img src="./assets/images/home/i--marker.png" alt="#">
    </a>
</section>