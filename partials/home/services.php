<section class="homeServices">
    <div class="container">
        <div class="homeServices__label">Dịch vụ</div>
        <div class="homeServices__list clearfix">
            <div class="js--homeServices__slide swiper-container">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="servicesItem">
                            <a class="servicesItem__hyperlink" href="#">
                                <div class="servicesItem__img"><img src="./assets/images/home/services-demo1.jpg" alt="TRUYỀN HÌNH CÁP HD"></div>
                                <h3 class="servicesItem__title">Truyền hình cáp HD</h3>
                                <div class="servicesItem__excerpt">Trang bị 01 đầu thu miễn phí Lắp đặt nhanh chóng trong 24h Chất lượng truyền hình HD Đa dạng nhóm kênh 195 kênh</div>
                            </a>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="servicesItem">
                            <a class="servicesItem__hyperlink" href="#">
                                <div class="servicesItem__img"><img src="./assets/images/home/services-demo2.jpg" alt="Internet"></div>
                                <h3 class="servicesItem__title">Internet</h3>
                                <div class="servicesItem__excerpt">Trang bị 02 đầu thu miễn phí Lắp đặt nhanh chóng trong 24h Chất lượng truyền hình HD Đa dạng nhóm kênh 210 kênh</div>
                            </a>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="servicesItem">
                            <a class="servicesItem__hyperlink" href="#">
                                <div class="servicesItem__img"><img src="./assets/images/home/services-demo3.jpg" alt="Internet + Truyền hình"></div>
                                <h3 class="servicesItem__title">Internet + truyền hình</h3>
                                <div class="servicesItem__excerpt">Trang bị 01 đầu thu miễn phí Lắp đặt nhanh chóng trong 24h Chất lượng truyền hình HD Đa dạng nhóm kênh 195 kênh</div>
                            </a>
                        </div>
                    </div>

                    <div class="swiper-slide">
                        <div class="servicesItem">
                            <a class="servicesItem__hyperlink" href="#">
                                <div class="servicesItem__img"><img src="./assets/images/home/services-demo4.jpg" alt="VTVCAB ON"></div>
                                <h3 class="servicesItem__title">VTVCAB ON</h3>
                                <div class="servicesItem__excerpt">Trang bị 01 đầu thu miễn phí Lắp đặt nhanh chóng trong 24h Chất lượng truyền hình HD Đa dạng nhóm kênh 195 kênh</div>
                            </a>
                        </div>
                    </div>

                </div> <!-- end swiper-wrapper -->
            </div> <!-- end swiper-container -->
            
            <div class="homeServices__controls show-devide-768">
                <a href="#" class="js--homeServices__slide__prev">
                    <i class="fa fa-angle-left"></i>
                </a>
                <a href="#" class="js--homeServices__slide__next">
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
        </div> <!-- end homeServices__list -->
    </div>

    <script>
        jQuery(document).ready(function(){
            var service_slide = new Swiper('.js--homeServices__slide', {
                slidesPerView: 4,
                spaceBetween: 0,
                breakpoints: {
                    768: {
                        slidesPerView: 2,
                    },
                    410: {
                        slidesPerView: 1
                    }
                }
            });
        });
    </script>
</section>