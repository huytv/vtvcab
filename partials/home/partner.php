<section class="homePartner">
    <div class="container">
        <div class="homePartner__label">Đối tác</div>

        <div class="js-homePartner__slide swiper-container">
            <div class="swiper-wrapper">
                <?php for($i = 0; $i < 2; $i++) : ?>
                <div class="swiper-slide">
                    <a href="#" class="homePartner__item"><img src="./assets/images/home/doi-tac-demo1.png" alt="ten cong ty"></a>
                </div>
                <div class="swiper-slide">
                    <a href="#" class="homePartner__item"><img src="./assets/images/home/doi-tac-demo2.png" alt="ten cong ty"></a>
                </div>
                <div class="swiper-slide">
                    <a href="#" class="homePartner__item"><img src="./assets/images/home/doi-tac-demo3.png" alt="ten cong ty"></a>
                </div>
                <div class="swiper-slide">
                    <a href="#" class="homePartner__item"><img src="./assets/images/home/doi-tac-demo4.png" alt="ten cong ty"></a>
                </div>
                <div class="swiper-slide">
                    <a href="#" class="homePartner__item"><img src="./assets/images/home/doi-tac-demo5.png" alt="ten cong ty"></a>
                </div>
                <?php endfor; ?>
            </div>
        </div><!-- end swiper-container -->

        <script>
            jQuery(document).ready(function(){
                var partner_slide = new Swiper('.js-homePartner__slide', {
                    slidesPerView: 5,
                    spaceBetween: 20,
                    loop: true,
                    autoplay: 3000,
                    speed: 1200,
                    breakpoints: {
                        768: {
                            slidesPerView: 2,
                        },
                        410: {
                            slidesPerView: 1
                        }
                    }
                });
            });
        </script>
    </div>
</section>