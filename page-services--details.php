<?php include_once('./layouts/header.php'); ?>

<?php include_once('./layouts/page-banner.php'); ?>
<main class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php include_once('./layouts/sidebar--page-services.php'); ?>
            </div>
            <div class="col-md-8 col-md-offset-1">
                <div class="article__wrapper">
                    <div class="article__title">TRUYỀN HÌNH CÁP HD</div>  
                    <div class="article__content">
                        Nội dung chi tiết dịch vụ
                        <a href="./page-services--register.php" class="register-service__btn">Đăng ký ngay</a>
                    </div>
                </div>
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</main>

<?php include_once('./layouts/footer.php'); ?>
