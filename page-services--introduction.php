<?php include_once('./layouts/header.php'); ?>

<?php include_once('./layouts/page-banner.php'); ?>
<main class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php include_once('./layouts/sidebar--page-services.php'); ?>
            </div>
            <div class="col-md-8 col-md-offset-1">
                <div class="article__wrapper">
                    <div class="article__title">Dịch vụ</div>  
                    <div class="article__content">
                        Bằng khát vọng tiên phong cùng chiến lược - đầu tư bền vững, VTVcab hướng đến là nhà cung cấp đa dịch vụ, là dịch vụ thiết yếu trong mỗi gia đình, là người bạn thân thiết luôn đồng hành cùng mỗi khán giả.
                        Dịch vụ truyền hình trả tiền
                        - Truyền hình cáp
                        - Truyền hình số
                        + Truyền hình số độ nét tiêu chuẩn
                        + Truyền hình số độ nét cao HD
                        - Truyền hình theo yêu cầu VTVcab ON
                        Dịch vụ internet 
                        Dịch vụ quảng cáo
                    </div>
                </div>
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</main>

<?php include_once('./layouts/footer.php'); ?>
