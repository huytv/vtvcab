    <footer class="footer">
        <div class="container">
            <div class="footer__top clearfix">
                <div class="col-md-3">
                    <a href="#" class="footer__logo"><img src="./assets/images/logo.png" alt="Vtvcab Đồng Nai"></a>
                    <div class="footer__network-social tac">
                        <a href="#" class="footer__network-social__item">
                            <i class="fa fa-youtube"></i>
                        </a>
                        <a href="#" class="footer__network-social__item">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a href="#" class="footer__network-social__item">
                            <i class="fa fa-map-o"></i>
                        </a>
                    </div>
                    <p class="footer__textLine">
                        <i class="fa fa-map-marker" aria-hidden="true"></i><span>Số 159 khu Phố 6 đường Đồng Khởi thành phố Biên Hoà - tỉnh Đồng Nai.</span>
                    </p>
                    <div class="footer__text">
                        <p class="footer__textLine"><i class="fa fa-phone" aria-hidden="true"></i><a href="tel://0462656566">0933708027</a></p>
                        <p class="footer__textLine"><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:kinhtho@vtvcab.vn">kinhtho@vtvcab.vn</a></p>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-1">
                    <div class="footer__label">Chi nhánh</div>
                    <div class="footer__text">
                        <p class="footer__textLine"><span class="footer__textLine__label">Tân Phú</span> <span class="footer__textLine__content">Số 159 khu Phố 6 đường Đồng Khởi thành phố Biên Hoà - tỉnh Đồng Nai.</span></p>
                    </div>
                    <div class="footer__text">
                        <p class="footer__textLine"><span class="footer__textLine__label">Tân Phú</span> <span class="footer__textLine__content">Số 159 khu Phố 6 đường Đồng Khởi thành phố Biên Hoà - tỉnh Đồng Nai.</span></p>
                    </div>
                    <div class="footer__text">
                        <p class="footer__textLine"><span class="footer__textLine__label">Tân Phú</span> <span class="footer__textLine__content">Số 159 khu Phố 6 đường Đồng Khởi thành phố Biên Hoà - tỉnh Đồng Nai.</span></p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer__label">Liên kết nhanh</div>
                    <div class="footer__text">
                        <a href="#" class="footer__hyperlink">VTVCab ON</a>
                        <a href="#" class="footer__hyperlink">Internet</a>
                        <a href="#" class="footer__hyperlink">Internet + Truyền hình</a>
                        <a href="#" class="footer__hyperlink">Truyền hình cáp HD</a>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="footer__label">Theo dõi chúng tôi</div>
                    <div>facebook embed</div>
                </div>
            </div>
        </div>
        <div class="footer__bottom">@ 2017 VTVCAB. All right reserved.</div>
    </footer>
    
</body>
</html>