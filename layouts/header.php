<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <meta name="author" content="" />
    <meta name="description" content="" />

    <title>Truyền hình cáp Việt Nam | Chi nhánh Đồng Nai</title>

    <link type="image/x-icon" rel="shortcut icon" href="./assets/images/favicon.png?v=0.1">

    <script src="./assets/bower_components/jquery/jquery.min.js"></script>
    <script src="./assets/bower_components/Swiper/dist/js/swiper.jquery.min.js"></script>
    <script src="./assets/bower_components/aos/dist/aos.js"></script>
    <script src="./assets/bower_components/fancybox/dist/jquery.fancybox.min.js"></script>
    <script src="./assets/scripts/main.js"></script>

    <link rel="stylesheet" href="./assets/bower_components/Swiper/dist/css/swiper.min.css">
    <link rel="stylesheet" href="./assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/bower_components/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="./assets/bower_components/aos/dist/aos.css">
    <link rel="stylesheet" href="./assets/bower_components/fancybox/dist/jquery.fancybox.css">
    <link rel="stylesheet" href="./assets/stylesheets/main.css">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,700&amp;subset=vietnamese" rel="stylesheet">
</head>
<body>

<!--<a href="./page-services--register.php" class="register-service__btn--fixed">Đăng ký dịch vụ</a>-->
<?php //include('./partials/block--network-socials.php'); ?>

<header class="js-scrollspy header">
    <section class="header__top hidden-devide-768">
        <div class="container">
            <div class="row">
                <div class="header__network-social fl">
                    <a href="#" class="header__network-social__item">
                        <i class="fa fa-youtube"></i>
                    </a>
                    <a href="#" class="header__network-social__item">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="#" class="header__network-social__item">
                        <i class="fa fa-map-o"></i>
                    </a>
                </div>
                <div class="fr">
                    <a href="#" class="header__support">
                        <img src="./assets/images/icons/i--supporter.png" alt="#">
                        <span>Hỗ trợ khách hàng</span>
                    </a>
                    <a href="tel:0123456789" class="header__hotline">
                        <i class="fa fa-phone"></i>
                        <span>19001515</span>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <div class="container clearfix">
        <div class="row">
            <a href="index.php" class="header__logo fl"><img src="./assets/images/logo2.png" alt="#"></a>

            <a href="#" class="js-headerHamburger hamburger fr show-devide-768">
                <span class="hamburger__line--one"></span>
                <span class="hamburger__line--two"></span>
                <span class="hamburger__line--three"></span>
            </a>
            
            <a href="" class="networksocial__item show-devide-768 fr facebook">
                <i class="fa fa-facebook"></i>
            </a>

            <a href="tel:01234567891" class="networksocial__item show-devide-768 fr phone">
                <i class="fa fa-phone"></i>
            </a>

            <ul class="headerMenu clearfix fr js-headerMenu">
                <li class="headerMenu__item is-active"><a href="index.php">Trang chủ</a></li>
                <li class="headerMenu__item"><a href="./page-introduction.php">Giới thiệu</a></li>
                <li class="headerMenu__item"><a href="./page-services--introduction.php">Dịch vụ</a></li>
                <li class="headerMenu__item"><a href="./page-services--register.php">Đăng ký dịch vụ</a></li>
                <li class="headerMenu__item"><a href="./page-news--list.php">Tin tức</a></li>
                <li class="headerMenu__item"><a href="#">Tuyển dụng</a></li>
                <li class="headerMenu__item"><a href="#">Liên hệ</a></li>
            </ul>

        </div>
    </div>
    <script>
        jQuery(document).ready(function($){
            if ($(window).width() > 768) {
                $(window).scroll(function(){
                    var scrollTop = $(window).scrollTop();

                    if (scrollTop > 200) {
                        $('header').addClass('is-fixed');
                        if (scrollTop > 500) {
                            $('header').addClass('is-show');
                        }else {
                            $('header').removeClass('is-show');
                        }
                    }else {
                        $('header').removeClass('is-fixed');
                    }
                    $('.js-scrollspy');
                });
            }
        });
    </script>
</header>
    
       
