<section class="banner">
    <div id="js-bannerSlide" class="swiper-container">
        <div class="swiper-wrapper">
            
            <div class="swiper-slide">
                <div class="banner__img">
                    <div class="inner">
                        <div class="container">
                            <div class="banner__text hidden-devide-768">
                                <h3 class="banner__title"><a href="#">Tốc độ cao hơn <br>Không tăng giá cước</a></h3>
                                <div class="banner__excerpt">Áp dụng cho khách hàng lắp mới đóng trước thuê bao trọn gói từ 3 tháng trở lên</div>
                                <a href="#" class="banner__btn">Tìm hiểu thêm</a>
                            </div>
                        </div>
                    </div>
                    <img src="./assets/images/home/banner-demo2.png" alt="">
                </div>
            </div>

            <div class="swiper-slide">
                <div class="banner__img">
                    <div class="inner">
                        <div class="container">
                            <div class="banner__text hidden-devide-768">
                                <h3 class="banner__title"><a href="#">Tốc độ cao hơn <br>Không tăng giá cước</a></h3>
                                <div class="banner__excerpt">Áp dụng cho khách hàng lắp mới đóng trước thuê bao trọn gói từ 3 tháng trở lên</div>
                                <a href="#" class="banner__btn">Tìm hiểu thêm</a>
                            </div>
                        </div>
                    </div>
                    <img src="./assets/images/home/banner-demo1.png" alt="">
                </div>
            </div>

        </div>
        <div class="swiper-pagination"></div>

        <div class="swiper-button-next"><i class="fa fa-angle-right"></i></div>
        <div class="swiper-button-prev"><i class="fa fa-angle-left"></i></div>
    </div>
</section>